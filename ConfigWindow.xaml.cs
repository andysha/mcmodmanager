using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.ComponentModel;

namespace McModManager
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow : Window
    {
        public ConfigWindow()
        {
            InitializeComponent();
            FolderBox.Text = Config.MinecraftPath;
            VersionBox.ItemsSource = Config.Versions;
            VersionBox.SelectedItem = Config.DefaultVersion;
            ModloaderBox.ItemsSource = Enum.GetValues(typeof(Modloader));
            ModloaderBox.SelectedItem = Config.DefaultModloader;
        }

        bool isSaved = false;
        private bool SaveConfig()
        {
            if (!Directory.Exists(FolderBox.Text))
            {
                MessageBox.Show("The specified folder does not exist", "Error");
                return false;
            }
            if (!Directory.Exists(Path.Combine(FolderBox.Text, "mods")))
            {
                MessageBox.Show("The specified folder does not contain mods folder", "Error");
                return false;
            }

            Config.MinecraftPath = FolderBox.Text;
            Config.DefaultVersion = (string) VersionBox.SelectedItem;
            Config.DefaultModloader = (Modloader) ModloaderBox.SelectedItem;

            if (File.Exists(Path.Combine(Config.MinecraftPath, "..", "..", "..", "multimc.cfg")))
                Config.IsMultiMC = true;
            else Config.IsMultiMC = false;

            Config.Save();
            isSaved = true;

            MessageBox.Show("Saved successfully!");
            return true;
        }

        private void SaveConfigButton_Click(object sender, EventArgs e)
        {
            if (SaveConfig()) Close();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (!isSaved)
            {
                MessageBoxResult res = MessageBox.Show("Do you want to save the configuration?", "", MessageBoxButton.YesNo);

                if (res == MessageBoxResult.Yes)
                {
                    if (!SaveConfig()) e.Cancel = true;
                }
            }
        }
    }
}

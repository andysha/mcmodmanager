using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace McModManager
{
    /// <summary>
    /// Interaction logic for ModPage.xaml
    /// </summary>
    public partial class ModPage : Window
    {
        public Mod Mod { get; set; }

        public ModPage(uint modId)
        {
            HttpWebRequest request = WebRequest.CreateHttp(@"https://addons-ecs.forgesvc.net/api/v2/addon/" + modId);
            string resp = "";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                resp = reader.ReadToEnd();
            }

            Mod = JsonSerializer.Deserialize<Mod>(resp);

            InitializeComponent();
        }
    }
}

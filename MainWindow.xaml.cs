using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

using System.Net;
using System.IO;
using System.Text.Json;

namespace McModManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Config.Load();
            VersionBox.ItemsSource = Config.Versions;
            VersionBox.SelectedItem = Config.DefaultVersion;
            ModloaderBox.ItemsSource = Enum.GetValues(typeof(Modloader));
            ModloaderBox.SelectedItem = Config.DefaultModloader;
        }

        public class ModDescription
        {
            public string Title { get; set; }
            public string Description { get; set; }
            public string Tags { get; set; }
            public BitmapImage Icon { get; set; }
            public uint Id { get; set; }
            public string FileLink { get; set; }
            public string FileName { get; set; }
            public SolidColorBrush Color { get; set; }
        }

        public ObservableCollection<ModDescription> ModList { get; set; } = new ObservableCollection<ModDescription>();
        
        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ModList.Clear();
            string url = @"https://addons-ecs.forgesvc.net/api/v2/addon/search?gameId=432&sectionId=6" +
                "&gameVersion=" + VersionBox.Text +
                (SearchBox.Text != "" ? ("&searchFilter=" + SearchBox.Text) : "");
            HttpWebRequest request = WebRequest.CreateHttp(url);
            string resp = "";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                resp = reader.ReadToEnd();
            }

            List<Mod> mods = JsonSerializer.Deserialize<List<Mod>>(resp);

            foreach (var mod in mods)
            {
                int index = 0;
                if ((index = mod.latestFiles.FindIndex(f => (f.gameVersion.FindIndex(v => v == ModloaderBox.Text) != -1 && f.gameVersion.FindIndex(v => v == VersionBox.Text) != -1))) != -1)
                {
                    ModDescription description = new ModDescription
                    {
                        Title = mod.name,
                        Description = mod.summary,
                        Tags = mod.latestFiles[index].displayName,
                        Icon = new BitmapImage(new Uri(mod.attachments[0].url)),
                        Id = mod.id.GetValueOrDefault(0),
                        FileLink = mod.latestFiles[index].downloadUrl,
                        FileName = mod.latestFiles[index].fileName
                    };

                    ModList.Add(description);
                }
            }

            
        }

        private void ListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                using WebClient web = new WebClient();
                ModDescription mod = (ModDescription)ListMods.SelectedItem;
                web.DownloadFile(mod.FileLink, Path.Combine(Config.MinecraftPath, "mods", mod.FileName));
                mod.Color = Brushes.LightGreen;
                ListMods.Items.Refresh();
            }
            catch (WebException ex)
            {
                if (ex.InnerException?.GetType() == typeof(DirectoryNotFoundException))
                {
                    MessageBox.Show("The Minecraft folder is not found", "Error");
                }
            }
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }

        private void ConfigButton_Click(object sender, RoutedEventArgs e) => 
            new ConfigWindow().Show();
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.IO;

namespace McModManager
{
    public enum Modloader
    {
        NA, Forge, Fabric
    }

    public static class Config
    {
        public static string MinecraftPath { get; set; } = "";
        public static bool IsMultiMC { get; set; } = false;
        public static string DefaultVersion { get; set; } = "1.16.5";
        public static Modloader DefaultModloader { get; set; } = Modloader.Forge;

        public static void Save()
        {
            ConfigFile config = new ConfigFile
            {
                MinecraftPath = MinecraftPath,
                IsMultiMC = IsMultiMC,
                DefaultVersion = DefaultVersion,
                DefaultModloader = DefaultModloader
            };
            string save = JsonSerializer.Serialize(config, typeof(ConfigFile));
            StreamWriter file = new StreamWriter("config.json");
            file.Write(save);
            file.Flush();
            file.Close();
        }

        public static void Load()
        {
            if (!File.Exists("config.json")) Save();
            StreamReader file = new StreamReader("config.json");
            string load = file.ReadToEnd();
            file.Close();

            ConfigFile config = (ConfigFile) JsonSerializer.Deserialize(load, typeof(ConfigFile));
            MinecraftPath = config.MinecraftPath;
            IsMultiMC = config.IsMultiMC;
            DefaultVersion = config.DefaultVersion;
            DefaultModloader = config.DefaultModloader;
        }

        class ConfigFile
        {
            public string MinecraftPath { get; set; } = "";
            public bool IsMultiMC { get; set; } = false;
            public string DefaultVersion { get; set; } = "";
            public Modloader DefaultModloader { get; set; } = Modloader.NA;
        }

        public static string[] Versions { get; } = { "1.16.5" };
    }
}

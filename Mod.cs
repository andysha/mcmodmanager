using System;
using System.Collections.Generic;
using System.Text;

namespace McModManager
{
#pragma warning disable IDE1006 // Naming Styles
#nullable enable
    // Root myDeserializedClass = JsonConvert.Deserializeobject?<Root>(myJsonResponse); 
    public class Author
    {
        public string? name { get; set; }
        public string? url { get; set; }
        public uint? projectId { get; set; }
        public uint? id { get; set; }
        public object? projectTitleId { get; set; }
        public object? projectTitleTitle { get; set; }
        public uint? userId { get; set; }
        public uint? twitchId { get; set; }
    }

    public class Attachment
    {
        public uint? id { get; set; }
        public uint? projectId { get; set; }
        public string? description { get; set; }
        public bool? isDefault { get; set; }
        public string? thumbnailUrl { get; set; }
        public string? title { get; set; }
        public string? url { get; set; }
        public uint? status { get; set; }
    }

    public class Module
    {
        public string? foldername { get; set; }
        public object? fingerprint { get; set; }
        public uint? type { get; set; }
    }

    public class SortableGameVersion
    {
        public string? gameVersionPadded { get; set; }
        public string? gameVersion { get; set; }
        public DateTime? gameVersionReleaseDate { get; set; }
        public string? gameVersionName { get; set; }
    }

    public class LatestFile
    {
        public uint? id { get; set; }
        public string? displayName { get; set; }
        public string? fileName { get; set; }
        public DateTime? fileDate { get; set; }
        public uint? fileLength { get; set; }
        public uint? releaseType { get; set; }
        public uint? fileStatus { get; set; }
        public string? downloadUrl { get; set; }
        public bool? isAlternate { get; set; }
        public uint? alternateFileId { get; set; }
        public List<object>? dependencies { get; set; }
        public bool? isAvailable { get; set; }
        public List<Module>? modules { get; set; }
        public object? packageFingerprint { get; set; }
        public List<string>? gameVersion { get; set; }
        public List<SortableGameVersion>? sortableGameVersion { get; set; }
        public object? installMetadata { get; set; }
        public object? changelog { get; set; }
        public bool? hasInstallScript { get; set; }
        public bool? isCompatibleWithClient { get; set; }
        public uint? categorySectionPackageType { get; set; }
        public uint? restrictProjectFileAccess { get; set; }
        public uint? projectStatus { get; set; }
        public uint? renderCacheId { get; set; }
        public object? fileLegacyMappingId { get; set; }
        public uint? projectId { get; set; }
        public object? parentProjectFileId { get; set; }
        public object? parentFileLegacyMappingId { get; set; }
        public object? fileTypeId { get; set; }
        public object? exposeAsAlternative { get; set; }
        public uint? packageFingerprintId { get; set; }
        public DateTime? gameVersionDateReleased { get; set; }
        public uint? gameVersionMappingId { get; set; }
        public uint? gameVersionId { get; set; }
        public uint? gameId { get; set; }
        public bool? isServerPack { get; set; }
        public object? serverPackFileId { get; set; }
        public object? gameVersionFlavor { get; set; }
    }

    public class Category
    {
        public uint? categoryId { get; set; }
        public string? name { get; set; }
        public string? url { get; set; }
        public string? avatarUrl { get; set; }
        public uint? parentId { get; set; }
        public uint? rootId { get; set; }
        public uint? projectId { get; set; }
        public uint? avatarId { get; set; }
        public uint? gameId { get; set; }
    }

    public class CategorySection
    {
        public uint? id { get; set; }
        public uint? gameId { get; set; }
        public string? name { get; set; }
        public uint? packageType { get; set; }
        public string? path { get; set; }
        public string? initialInclusionPattern { get; set; }
        public object? extraIncludePattern { get; set; }
        public uint? gameCategoryId { get; set; }
    }

    public class GameVersionLatestFile
    {
        public string? gameVersion { get; set; }
        public uint? projectFileId { get; set; }
        public string? projectFileName { get; set; }
        public uint? fileType { get; set; }
        public object? gameVersionFlavor { get; set; }
    }

    public class Mod
    {
        public uint? id { get; set; }
        public string? name { get; set; }
        public List<Author>? authors { get; set; }
        public List<Attachment>? attachments { get; set; }
        public string? websiteUrl { get; set; }
        public uint? gameId { get; set; }
        public string? summary { get; set; }
        public uint? defaultFileId { get; set; }
        public float? downloadCount { get; set; }
        public List<LatestFile>? latestFiles { get; set; }
        public List<Category>? categories { get; set; }
        public uint? status { get; set; }
        public uint? primaryCategoryId { get; set; }
        public CategorySection? categorySection { get; set; }
        public string? slug { get; set; }
        public List<GameVersionLatestFile>? gameVersionLatestFiles { get; set; }
        public bool? isFeatured { get; set; }
        public double popularityScore { get; set; }
        public uint? gamePopularityRank { get; set; }
        public string? primaryLanguage { get; set; }
        public string? gameSlug { get; set; }
        public string? gameName { get; set; }
        public string? portalName { get; set; }
        public DateTime? dateModified { get; set; }
        public DateTime? dateCreated { get; set; }
        public DateTime? dateReleased { get; set; }
        public bool? isAvailable { get; set; }
        public bool? isExperiemental { get; set; }
    }

#nullable disable
#pragma warning restore IDE1006 // Naming Styles
}
